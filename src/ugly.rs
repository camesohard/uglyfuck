use std::env;
use std::fs;
use std::io::Read;
use std::process::exit;

enum Commands {
	Increment,
	Decrement,
	IncrementPointer,
	DecrementPointer,
	Read,
	Write,
}

// Lexer turns known strings to tokens
fn lexer(command_list: String) -> Vec<Commands> {
	let mut command_lexer: Vec<Commands> = Vec::new();
	for i in command_list.chars() {
		match i {
			'+' => command_lexer.push(Commands::Increment),
			'-' => command_lexer.push(Commands::Decrement),
			'>' => command_lexer.push(Commands::IncrementPointer),
			'<' => command_lexer.push(Commands::DecrementPointer),
			'.' => command_lexer.push(Commands::Write),
			'*' => command_lexer.push(Commands::Read),
			_ => (),
		}
	}

	command_lexer
}

// Runner takes converted tokens and does operations on them
fn runner(commands: &Vec<Commands>, cell: &mut Vec<u8>, cell_pointer: &mut usize) {
	let mut column = 0; // Log column to help developer spot error quickly

	for i in commands {
		column += 1;
		match i {
			Commands::IncrementPointer => {
				if cell_pointer > &mut (1024 as usize) {
					panic!(
						"{:?} {:?}: {:?}",
						"Error at column", column, "You are pointing outside of cell range!"
					);
				}
				*cell_pointer += 1
			}
			Commands::DecrementPointer => {
				println!("{:?}", cell_pointer);
				if cell_pointer > &mut (1024 as usize) {
					panic!(
						"{:?} {:?}: {:?}",
						"Error at column", column, "You are pointing outside of cell range!"
					);
				}
				*cell_pointer -= 1
			}
			Commands::Increment => cell[*cell_pointer] += 1,
			Commands::Decrement => cell[*cell_pointer] -= 1,
			Commands::Read => {
				let mut buffer: [u8; 1] = [0; 1];

				// Read exactly one bite from stdin
				std::io::stdin().read_exact(&mut buffer).expect("Error! Couldn't read stdin.");
				cell[*cell_pointer] = buffer[0];

			},
			Commands::Write => print!("{}", cell[*cell_pointer] as char),
		};
	}
}

fn main() {
	let args: Vec<String> = env::args().collect();

	if args.len() < 2 {
		println!("{:?}", "usage: ugly [FILE_NAME]");
		exit(1)
	}
	let file = &args[1];

	let code = fs::read_to_string(file).expect("Something went wrong while reading the file");

	let commands: Vec<Commands> = lexer(code);
	let mut cell: Vec<u8> = vec![0; 1024];
	let mut cell_pointer: usize = 512;

	runner(&commands, &mut cell, &mut cell_pointer);
}
